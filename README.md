# Blocking Semaphore
Manage blocking semaphores for bash scripts and crontab.
## Getting Started
To install this script copy it to somewhere in your path. Ie.
```
$ cp blksem /usr/local/bin
```

### Help

    $ blksem -h

    Manage blocking semaphores for bash scripts. (Ie. in crontab.)
    Usage: blksem [options] SEMAPHORE

    Command line switches are optional. The following switches are recognized.
      -s|--show      Show status of semaphore (default)
      -e|--enable    Enable the listet semaphores
      -d|--disable   Disable the listed semaphores
      -u|--until TS  Enable until timestamp. (Look up examples in DATE(1) option -d)
      -l|--list      List semaphores and there status
      --add          Add a semaphore definition
      --delete       Delete semaphore definition

    Options:
      -m|--msg TEXT  Describe a reason to add or enable a semaphore
      -S|--shared    Uses shared semaphore
      -f|--format FS Format: use one or more of 'DSRULB' for
                       Description, Status, Reason, Until, LastChange, ByUser
      -p             Parsable. Tab separated output
      -t             Prepend error text with timestamp
      -r             Returns exit code 1 if enabled
      -h|--help      Displays this help message

### Examples of use
Create a semaphore MySem with text explaining what it is used for
```
$ blksem --add MySem -m 'Semaphore used for test'
$ blksem MySem
Disabled
```

Enable the semaphore with a text explaining why
```
$ blksem -e MySem -m 'Test period of 1 hour' -u '1 hour'
$ blksem -s MySem
Enabled
```

List all the semaphores created, with all information.

```
$ blksem -l -f DSRULB

MySem
 - Description:	Semaphore used for test
 - Status:	Enabled
 - Reason:	Test period of 1 hour
 - Until:	2020-09-04T17:59:07+02:00
 - Last change:	2020-09-04T17:58:32+02:00
 - By user:	foobar
```
To get the line with _status, when, who, why_
```
$ semaphore_status=$(blksem -rpf SLBR MySem)
$ semaphore_disabled=$?
$ echo "$semaphore_status"|sed 's/\t/|/g'
Enabled|2020-09-04T17:59:07+02:00|foobar|Test period of 1 hour
$ echo $semaphore_disabled
1
```
The exit value is `0` for disabled and `1` for enabled.

Disable the semaphore
```
$ blksem -d MySem
```
And create a shared semaphore
```
$ sudo blksem --add -S SharedSem
```
Now every user can see the semaphore ie. block a line in crontab)
```
0 1 * * * blksem -rS SharedSem && echo 'not enabled' >> /dir/logfile 2>&1
```


### Exit values
1. Semaphore enabled
2. WARNING: Function allready set or show help
3. ERROR: Wrong options or input
4. ERROR: Program errors
5. ERROR: Lock file is locked

### Semaphore files
The semaphore are stored in `~/.blocking_semaphores`
or if it is shared in `/etc/blocking_semaphores`. If shared it is normally only root that can alter the semaphores.
When altered there will be a file lock on `~/.blocking_semaphores.lock` so even if more people alters semaphores simultaneously, will not overwrite the semaphore file.

### Environment variables
`SEMAPHORE_FILE`:  The path to an alternative  location for the semaphore file.

## Contributing
Your welcome to comment and make suggestions. I will decide what goes in the next version based on your comments.

<!---
## Versioning
We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/jehoj/bash_semaphore/tags). 
-->

## Authors
* **Niels Erik Jehøj-Krogager** - *Initial work* 

<!---
See also the list of [contributors](https:https://gitlab.com/jehoj/bash_semaphore/contributors) who participated in this project.
-->

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments
* Google is an efective way not to get stuck :innocent:
